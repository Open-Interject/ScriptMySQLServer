﻿# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 
#  NOTE: In order to run Powershell scripts, you need to enable them... There is a security vulnerability if web-based scripts are downloaded and executed
#  ...   if you see... MyFileName.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
#  ... Open PowerShell
#  ...		To see current execution policy:
#  ...           Get-ExecutionPolicy   
#  ...		To change execution policy for current powershell session: 
#  ...           Set-ExecutionPolicy -Scope Process -ExecutionPolicy ByPass -Force
#  ...		IMPORTANT: This will only affect the execution policy for you current powershell session. 
#
#  NOTE: If you clone this repository using git then you should be able to skip the above step. The above error only happens when you download the .zip file of this repository
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

### HOW TO USE ###
# In Command Prompt:
# 	powershell.exe -File "FolderPathToThisScript\ScriptMySQLServer.ps1" ParentFolder ServerInstance [Username] [Password]
#
# In Powershell:
#	.\ExecuteSqlScripts.ps1 ParentFolder ServerInstance Database [Username] [Password]
#
# Script Requirements: invoke-sqlcmd
#
### Parameters ###
# $Argument1 = ParentFolder 	-- the folder that contains SQL Object folders e.g. "Schema", "Table" ...
# $Argument2 = ServerInstance	-- Server to connect to
# $Argument3 = Username			-- username of server
# $Argument4 = Password			-- password of server

param($Argument1,$argument2,$argument3,$argument4,$argument5)
    $ParentFolder 	  = $argument1
    $ServerInstance   = $argument2
    $Username		  = $argument3
    $Password		  = $argument4
    $Database         = $argument5  # if blank will pull all dbs. Case sensitive 

$ErrorActionPreference = "silentlyContinue"

#ToDo : Modify script to execute based on a payload file

#Connecting to database(s)
$MySQL_Conn = " --host=""$ServerInstance"" --user=""$Username"" --password=""$Password"" $Database" 

# When changing script set to 1 to see varables
$TestMode = 0

#$results = New-Object System.Object


# loop through all files and execute
Foreach ( $File IN Get-ChildItem -path $ParentFolder -recurse -Include '*.sql' )
{

    $FileName = $File.FullName
    $result = cmd  /c "mysql $MySQL_Conn < $FileName " 
    

    if ($TestMode -eq 1)
    {echo ("mysql $MySQL_Conn < $File ") }
    
    echo ($File)
    #break

}












