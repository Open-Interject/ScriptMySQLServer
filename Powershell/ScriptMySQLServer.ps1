# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 
#  NOTE: In order to run Powershell scripts, you need to enable them... There is a security vulnerability if web-based scripts are downloaded and executed
#  ...   if you see... MyFileName.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
#  ... Open PowerShell
#  ...		To see current execution policy:
#  ...           Get-ExecutionPolicy   
#  ...		To change execution policy for current powershell session: 
#  ...           Set-ExecutionPolicy -Scope Process -ExecutionPolicy ByPass -Force
#  ...		IMPORTANT: This will only affect the execution policy for you current powershell session. 
#
#  NOTE: If you clone this repository using git then you should be able to skip the above step. The above error only happens when you download the .zip file of this repository
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

### HOW TO USE ###
# In Command Prompt:
# 	powershell.exe -File "FolderPathToThisScript\ScriptMySQLServer.ps1" ParentFolder ServerInstance [Username] [Password]
#
# In Powershell:
#	.\ExecuteSqlScripts.ps1 ParentFolder ServerInstance Database [Username] [Password]
#
# Script Requirements: invoke-sqlcmd
#
### Parameters ###
# $Argument1 = ParentFolder 	-- the folder that contains SQL Object folders e.g. "Schema", "Table" ...
# $Argument2 = ServerInstance	-- Server to connect to
# $Argument3 = Username			-- username of server
# $Argument4 = Password			-- password of server

param($Argument1,$argument2,$argument3,$argument4,$argument5)

$ParentFolder 	  = $argument1
$ServerInstance   = $argument2
$Username		  = $argument3
$Password		  = $argument4
$LimitToDB        = $argument5  # if blank will pull all dbs. Case sensitive 

#Connecting to database(s)
$MySQL_Conn = " --host=""$ServerInstance"" --user=""$Username"" --password=""$Password"""
$DumpFolderLocation = "$ParentFolder"

# When changing script set to 1 to see varables
$TestMode = 0

############################################################
# Get all sps and functions
############################################################
# Get all procedure and function in db

if ($LimitToDB -eq $null){$LimitToDB = ''}

IF ($LimitToDB -eq '') 
    {$SQLStmt ='" SELECT CONCAT(type,''@'',db,''.'',name) FROM mysql.proc WHERE db != ''sys''; " '} 
ELSE 
    {$SQLStmt ='" SELECT CONCAT(type,''@'',db,''.'',name) FROM mysql.proc WHERE db  = ''' + $LimitToDB + '''; " '} 


# Execute MySQL server 
$SpList = iex "mysql $MySQL_Conn -ANe $SQLStmt"

$ProcCount = $SpList.Count;

if ($ProcCount -eq 0 ) 
	{ then exit }; 


############################################################
# Script each sps or/and functions
############################################################
# loop over each sp or function and script out
foreach ($TypeDBSp in $SpList )
{

    # Print varables for testing 
    if ($TestMode -eq 1)
    {
        $TypeDBSp
    }

    #parse sps to create folder and file names
    $isSplitDBSp = $TypeDBSp -match '^(\S+)@(\S+)\.(\S+)'
    
    if (-Not $isSplitDBSp)
    {
        echo 'Exit no matching db or sp'
        break 
    }


    # Print varables for testing 
    if ($TestMode -eq 1)
    {
        # $TypeDBSp -match '^(\S+)@(\S+)\.(\S+)'
        $Matches
    }

    $ScriptType = $Matches[1]
    $DB= $Matches[2]
    $SP= $Matches[3]

    # Print varables for testing 
    if ($TestMode -eq 1)
    {
        $DB
        $SP
    }

    # Prep for MySQL
    $TypeDBSp = $TypeDBSp -replace "@", " ";

    # MySQL Script to generate sps contant 
	$SQLStmt= """SHOW CREATE $TypeDBSp\G;""" 
    
    # Execute to get sps contant 
    $SPFileText = iex "mysql $MySQL_Conn -ANe $SQLStmt"
    
    # Count sps or function lines
    $LineCount = $SPFileText.Length

    #Removed old count since it did not count all the lines
    #$LineCount = $SPFileText | Measure-Object �Line

    
    # Print varables for testing 
    if ($TestMode -eq 1)
    {
        $LineCount
     }
    
    # Test if file is greater then 6 lines
    if ($LineCount -gt 5)
    {

        # Trim remove 3 top and bottom lines
        $SPFileText = $SPFileText | SELECT -First ($LineCount - 3 ) | SELECT -Last  ($LineCount - 6 )
            
            
        if (-Not (Test-Path "$DumpFolderLocation\$DB")) 
        {New-Item -ItemType Directory -Force -Path "$DumpFolderLocation\$DB"}
               
        # Create to folder if the folder does not exists
        if (-Not (Test-Path "$DumpFolderLocation\$DB\$ScriptType"))
        {New-Item -ItemType Directory -Force -Path "$DumpFolderLocation\$DB\$ScriptType"} 
        
        # Add header and footer to drop create 
        $HeaderAddLines = "`r`n" + "DROP $ScriptType IF EXISTS $SP;" + "`r`n" + 'DELIMITER $$ ' + "`r`n" + "`r`n"
        $FooterAddLines = '$$' + "`r`n" + "`r`n" +  "DELIMITER ;" + "`r`n" + "`r`n"
        $SPFileText = $HeaderAddLines + ( ($SPFileText) -join "`r`n") + $FooterAddLines

        # Create folders and insert data into files 
        $SPFileText -replace "\\n", "`n"  > "$DumpFolderLocation\$DB\$ScriptType\$SP.sql"

        (Get-Content -path "$DumpFolderLocation\$DB\$ScriptType\$SP.sql") | Set-Content -Encoding UTF8 -Path "$DumpFolderLocation\$DB\$ScriptType\$SP.sql"

    }


}
